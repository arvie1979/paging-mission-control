# Paging Mission Control Solution
## Assumptions
- Data is coming in the right chronological order - there is no need to sort data by the timestamp;
- Alerts should be fired as soon as they appeared chronologicaly, not sorted by the component or by the satelite;
- Timestamp of the first violation condition is returned in the output.

## Extensibility points
- It supports multiple satellites
- New supported components can be added easily
- New other alerts types (yellow, for example) can be also added in the configuration

## Prerequisites
- I used Java JDK 20 for testing the solution
- Maven 3+ is required
- Internet access to maven repositories

## Runtime dependencies
- Latest Jackson Databind to serialize in-memory objects to JSON

## How to run the solution?
1. Build everything in one jar with all dependencies and run unit tests:
```
mvn clean install
```
2. Run the solution after the build (sample1.txt input file):
```
java -jar target/paging-mission-control-1.0-SNAPSHOT-jar-with-dependencies.jar src/test/sample1.txt
```

## Additional information
More details about the implementation can be found in the code comments.