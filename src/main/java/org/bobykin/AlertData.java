package org.bobykin;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

// Output data class for JSON dumper
public final class AlertData {
    private final int satelliteId;
    private final String severity;
    private final String component;
    private final String timestamp;

    public AlertData(int satelliteId, String severity, String component, LocalDateTime timestamp) {
        this.satelliteId = satelliteId;
        this.severity = severity;
        this.component = component;
        this.timestamp = timestamp.atOffset(ZoneOffset.UTC).toString(); // UTC time
    }

    public int getSatelliteId() {
        return satelliteId;
    }


    public String getSeverity() {
        return severity;
    }


    public String getComponent() {
        return component;
    }

    public String getTimestamp() {
        return timestamp;
    }

}
