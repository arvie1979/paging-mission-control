package org.bobykin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.function.Consumer;
import java.util.Set;

// Creates BATT or TSTAT monitor having telemetry data
public class MonitorController {
    static HashMap<String, AlertConfiguration[]> config = new HashMap<>();
    // HashMap of alert monitors - current implementation contains only BATT and TSTAT
    // It supports more than two satellites if needed
    static HashMap<String, AlertMonitor> monitors;

    static {
        // Configuration definition
        AlertConfiguration[] battConfig = {
                new AlertConfiguration(3, 300, "RED LOW", data->data.getRawValue()<data.getRedLow())
        };
        // insert more alert types for BATT component here
        AlertConfiguration[] tstatConfig = {
                new AlertConfiguration(3, 300, "RED HIGH", data->data.getRawValue()>data.getRedHigh())
        };
        // insert more alert types for TSTAT component here
        config.put("BATT", battConfig);
        config.put("TSTAT", tstatConfig);
        // this configuration can be extended with yellow alerts and more alerts for each component
    }

    public static void init() {
        monitors = new HashMap<>();
    }
    public static void handler(TelemetryData data, Consumer<AlertData> consumer) {
        AlertConfiguration[] alertTypes = getAlertTypes(data);
        for (AlertConfiguration alertType: alertTypes) {
            String key = String.format("%d-%s-%s", data.getSatelliteId(), alertType.severity(), data.getComponent());
            if (!monitors.containsKey(key)) {
                // create new monitor
                monitors.put(key, getInstance(data, alertType, consumer));
            }
            AlertMonitor monitor = monitors.get(key);
            // launch monitoring for the satellite-component-alert pair
            monitor.monitor(data);
        }
    }

    public static AlertConfiguration[] getAlertTypes(TelemetryData data) {
        if(config.containsKey(data.getComponent())) {
            return config.get(data.getComponent());
        } else {
            System.out.println("Unknown component: " + data.getComponent());
        }
        return new AlertConfiguration[]{};
    }

    public static AlertMonitor getInstance(TelemetryData data, AlertConfiguration alertType, Consumer<AlertData> consumer) {
        return new AlertMonitor(
                data.getSatelliteId(),
                data.getComponent(),
                alertType,
                consumer
        );
    }
}
