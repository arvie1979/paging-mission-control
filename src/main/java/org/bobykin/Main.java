package org.bobykin;

import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Stream;

// Main class used for the project execution with main function
// Reads the file line by line and stream the data to BATT and TSTAT monitors
// Collects alerts and prints pretty-formatted JSON back to the user
public class Main {
    // list of alerts for paging
    static ArrayList<AlertData> alerts;
    public static void main(String[] args) throws FileNotFoundException {
        if (args.length < 1) {
            System.out.println("Please provide the input file.");
            return;
        }
        // init logic
        alerts = new ArrayList<>();
        MonitorController.init(); // initialize monitors
        try (Stream<String> lines = Files.lines(Paths.get(args[0]))){
            lines.forEach(Main::handler);
        } catch (IOException e) {
            System.out.println("Specified input file not found.");
            return;
        }

        renderResults();
    }

    public static void handler(String line) {
        try {
            TelemetryData data = new TelemetryData(line);
            MonitorController.handler(data, alert -> alerts.add(alert));
        } catch (Exception e) {
            // skipping line parsing errors
            System.out.println("Line cannot be parsed: " + line);
        }
    }

    // render results as pretty-formatted JSON with right indents
    public static void renderResults() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            DefaultPrettyPrinter printer = new DefaultPrettyPrinter();
            DefaultPrettyPrinter.Indenter indenter =
                    new DefaultIndenter("    ", DefaultIndenter.SYS_LF);
            printer.indentObjectsWith(indenter);
            printer.indentArraysWith(indenter);
            String result = objectMapper.writer(printer).writeValueAsString(alerts);
            System.out.println(result);
        } catch (IOException e) {
            System.out.println("Error happened while converting data to JSON.");
        }
    }

}