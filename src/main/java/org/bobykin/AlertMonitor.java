package org.bobykin;

import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class AlertMonitor {
    private int count; // threshold (number of violation during duration)
    private int durationSeconds; // window size in seconds
    private int satelliteId;
    private String component;
    private String severity;

    private Consumer<AlertData> consumer; // callback
    private Predicate<TelemetryData> predicate; // alert condition
    private LinkedList<TelemetryData> window = new LinkedList<>();
    private boolean firedFirstAlert = false; // first alert should be fired sooner than duration
    public AlertMonitor(int satelliteId, String component, AlertConfiguration config, Consumer<AlertData> consumer) {
        this.count = config.count();
        this.durationSeconds = config.durationSeconds();
        this.consumer = consumer;
        this.satelliteId = satelliteId;
        this.component = component;
        this.severity = config.severity();
        this.predicate = config.predicate();
    }

    public String getSeverity() {
        return severity;
    }
    public void monitor(TelemetryData data) {
        // skip data for different satellites/components
        if (data.getSatelliteId() != satelliteId || !component.equals(data.getComponent())) {
            return;
        }
        // if not a violation condition - skip
        if(!predicate.test(data)) {
            return;
        }
        window.addLast(data);
        boolean windowChanged = false;
        // Adjusting the window size to be in duration
        while(ChronoUnit.SECONDS.between(
                window.peekFirst().getTimestamp(), window.peekLast().getTimestamp()) > durationSeconds) {
            window.removeFirst();
            windowChanged = true;
        }
        // main alert condition
        if((!firedFirstAlert || windowChanged) && window.size() >= count) {
            AlertData alert = new AlertData(
                    data.getSatelliteId(),
                    getSeverity(),
                    data.getComponent(),
                    window.getFirst().getTimestamp()
            );
            // callback
            consumer.accept(alert);
            firedFirstAlert = true;
        }
    }


}
