package org.bobykin;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public final class TelemetryData {
    private final int satelliteId;
    private final double redHigh;
    private final double redLow;
    private final double yellowHigh;
    private final double yellowLow;
    private final double rawValue;
    private final String component;
    private final LocalDateTime timestamp;

    // Parser of string
    public TelemetryData(String telemetry) throws Exception
    {
        String[] split = telemetry.split("\\|");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd HH:mm:ss.SSS");
        this.timestamp = LocalDateTime.parse(split[0], formatter);
        this.satelliteId = Integer.parseInt(split[1]);
        this.redHigh = Double.parseDouble(split[2]);
        this.yellowHigh = Double.parseDouble(split[3]);
        this.yellowLow = Double.parseDouble(split[4]);
        this.redLow = Double.parseDouble(split[5]);
        this.rawValue = Double.parseDouble(split[6]);
        this.component = split[7];
    }

    public int getSatelliteId() {
        return satelliteId;
    }

    public double getRedHigh() {
        return redHigh;
    }

    public double getRedLow() {
        return redLow;
    }

    public double getYellowHigh() {
        return yellowHigh;
    }

    public double getYellowLow() {
        return yellowLow;
    }

    public double getRawValue() {
        return rawValue;
    }

    public String getComponent() {
        return component;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

}
