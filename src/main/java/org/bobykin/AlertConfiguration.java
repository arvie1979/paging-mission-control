package org.bobykin;

import java.util.function.Predicate;

// Describes alert type configuration
public record AlertConfiguration (
        int count,
        int durationSeconds,
        String severity,
        Predicate<TelemetryData> predicate) {}
