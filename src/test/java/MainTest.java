import org.bobykin.Main;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;


public class MainTest {
    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
    @BeforeEach
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }
    @Test
    public void givenSample1OutputShouldMatch() {
        String[] a = {"src/test/sample1.txt"};
        try {
            Main.main(a);
            StringWriter expectedStringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(expectedStringWriter);
            printWriter.println("[");
            printWriter.println("    {");
            printWriter.println("        \"satelliteId\" : 1000,");
            printWriter.println("        \"severity\" : \"RED HIGH\",");
            printWriter.println("        \"component\" : \"TSTAT\",");
            printWriter.println("        \"timestamp\" : \"2018-01-01T23:01:38.001Z\"");
            printWriter.println("    },");
            printWriter.println("    {");
            printWriter.println("        \"satelliteId\" : 1000,");
            printWriter.println("        \"severity\" : \"RED LOW\",");
            printWriter.println("        \"component\" : \"BATT\",");
            printWriter.println("        \"timestamp\" : \"2018-01-01T23:01:09.521Z\"");
            printWriter.println("    }");
            printWriter.println("]");
            assertEquals(expectedStringWriter.toString(), outputStreamCaptor.toString());
        } catch (FileNotFoundException e) {
            fail("File not found");
        }
    }

    @Test
    public void givenSample3WithParseErrorsOutputShouldMatch() {
        String[] a = {"src/test/sample3.txt"};
        try {
            Main.main(a);
            StringWriter expectedStringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(expectedStringWriter);
            printWriter.println("Line cannot be parsed: 20180101 23:04:11.531~1000~17|15|9|8|7.9|BATT");
            printWriter.println("Line cannot be parsed: 20180101 23:05:05.021~1001~101|98|25|20|89.9|TSTAT");
            printWriter.println("[");
            printWriter.println("    {");
            printWriter.println("        \"satelliteId\" : 1000,");
            printWriter.println("        \"severity\" : \"RED HIGH\",");
            printWriter.println("        \"component\" : \"TSTAT\",");
            printWriter.println("        \"timestamp\" : \"2018-01-01T23:01:38.001Z\"");
            printWriter.println("    },");
            printWriter.println("    {");
            printWriter.println("        \"satelliteId\" : 1001,");
            printWriter.println("        \"severity\" : \"RED HIGH\",");
            printWriter.println("        \"component\" : \"TSTAT\",");
            printWriter.println("        \"timestamp\" : \"2023-07-01T23:01:38.001Z\"");
            printWriter.println("    },");
            printWriter.println("    {");
            printWriter.println("        \"satelliteId\" : 1001,");
            printWriter.println("        \"severity\" : \"RED LOW\",");
            printWriter.println("        \"component\" : \"BATT\",");
            printWriter.println("        \"timestamp\" : \"2023-07-01T23:01:09.521Z\"");
            printWriter.println("    }");
            printWriter.println("]");
            assertEquals(expectedStringWriter.toString(), outputStreamCaptor.toString());
        } catch (FileNotFoundException e) {
            fail("File not found");
        }
    }
    @Test
    public void notParametersShouldMatch() {
        try {
            Main.main(new String[0]);
            StringWriter expectedStringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(expectedStringWriter);
            printWriter.println("Please provide the input file.");
            assertEquals(expectedStringWriter.toString(), outputStreamCaptor.toString());
        } catch (FileNotFoundException e) {
            fail("File not found");
        }
    }

    @Test
    public void notFoundFileShouldMatch() {
        try {
            String[] a = {"src/test/not_found_sample.txt"};
            Main.main(a);
            StringWriter expectedStringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(expectedStringWriter);
            printWriter.println("Specified input file not found.");
            assertEquals(expectedStringWriter.toString(), outputStreamCaptor.toString());
        } catch (FileNotFoundException e) {
            fail("File not found");
        }
    }

    @Test
    public void emptyFileShouldMatch() {
        try {
            String[] a = {"src/test/empty.txt"};
            Main.main(a);
            StringWriter expectedStringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(expectedStringWriter);
            printWriter.println("[ ]");
            assertEquals(expectedStringWriter.toString(), outputStreamCaptor.toString());
        } catch (FileNotFoundException e) {
            fail("File not found");
        }
    }
    @AfterEach
    public void tearDown() {
        System.setOut(standardOut);
    }
}
