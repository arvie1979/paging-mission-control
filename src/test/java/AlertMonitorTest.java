import org.bobykin.AlertConfiguration;
import org.bobykin.AlertData;
import org.bobykin.AlertMonitor;
import org.bobykin.TelemetryData;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class AlertMonitorTest {

    @Test
    public void alertsShouldAppear() {
        try {

            ArrayList<AlertData> a = new ArrayList<>();
            AlertMonitor m = new AlertMonitor(1000, "BATT", new AlertConfiguration(3, 300, "RED LOW", data->data.getRawValue()<data.getRedLow()), x->a.add(x));
            m.monitor(new TelemetryData("20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT"));
            assertEquals(0, a.size());
            m.monitor(new TelemetryData("20180101 23:02:11.302|1000|17|15|9|8|7.7|BATT"));
            assertEquals(0, a.size());
            m.monitor(new TelemetryData("20180101 23:04:11.531|1000|17|15|9|8|7.9|BATT"));
            assertEquals(1, a.size());
            AlertData alert = a.get(0);
            assertEquals(1000, alert.getSatelliteId());
            assertEquals("BATT", alert.getComponent());
            assertEquals("RED LOW", alert.getSeverity());
            assertEquals("2018-01-01T23:01:09.521Z", alert.getTimestamp());
            m.monitor(new TelemetryData("20180101 23:05:07.421|1001|17|15|9|8|7.9|BATT"));
            assertEquals(1, a.size());

        } catch (Exception e) {
            fail("Unexpected exception");
        }
    }
}
