import org.bobykin.AlertData;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AlertDataTest {
    @Test
    public void alertShouldWork() {
        LocalDateTime dt = LocalDateTime.now();
        AlertData alert = new AlertData(1234, "RED HIGH", "BATT", dt);
        assertEquals(1234, alert.getSatelliteId());
        assertEquals("RED HIGH", alert.getSeverity());
        assertEquals("BATT", alert.getComponent());
        assertEquals(dt.atOffset(ZoneOffset.UTC).toString(), alert.getTimestamp());
    }
}
