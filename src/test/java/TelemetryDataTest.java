import org.bobykin.TelemetryData;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class TelemetryDataTest {
    final double EPS = 1e-6;
    @Test
    public void parseShouldWork() {
        try {
            TelemetryData data = new TelemetryData("20180101 23:01:05.001|1001|101|98|25|20|19.9|TSTAT");
            assertEquals("2018-01-01T23:01:05.001", data.getTimestamp().toString());
            assertEquals(1001, data.getSatelliteId());
            assertEquals(19.9f, data.getRawValue(), EPS);
            assertEquals("TSTAT", data.getComponent());
            assertEquals(101f, data.getRedHigh(), EPS);
            assertEquals(20f, data.getRedLow(), EPS);
            assertEquals(98f, data.getYellowHigh(), EPS);
            assertEquals(25f, data.getYellowLow(), EPS);
        } catch (Exception e) {
            fail("Exception");
        }
    }

    @Test
    public void emptyStringShouldFail() {
        try {
            TelemetryData data = new TelemetryData("");
            fail();
        } catch (Exception e) {
            assertEquals(true, true);
        }
    }

    @Test
    public void wrongSeparatorShouldFail() {
        try {
            TelemetryData data = new TelemetryData("20180101 23:01:05.001+1001-101/98|25/20\19.9*TSTAT");
            fail();
        } catch (Exception e) {
            assertEquals(true, true);
        }
    }
}
