import org.bobykin.*;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class MonitorControllerTest {
    @Test
    public void alertsShouldAppear() {
        try {
            ArrayList<AlertData> a = new ArrayList<>();
            MonitorController.init();
            MonitorController.handler(new TelemetryData("20180101 23:01:09.521|1000|17|15|9|8|7.8|BATT"), x -> a.add(x));
            assertEquals(0, a.size());
            MonitorController.handler(new TelemetryData("20180101 23:02:11.302|1000|17|15|9|8|7.7|BATT"), x -> a.add(x));
            assertEquals(0, a.size());
            MonitorController.handler(new TelemetryData("20180101 23:04:11.531|1000|17|15|9|8|7.9|BATT"), x -> a.add(x));
            assertEquals(1, a.size());
            AlertData alert = a.get(0);
            assertEquals(1000, alert.getSatelliteId());
            assertEquals("BATT", alert.getComponent());
            assertEquals("RED LOW", alert.getSeverity());
            assertEquals("2018-01-01T23:01:09.521Z", alert.getTimestamp());
            MonitorController.handler(new TelemetryData("20180101 23:05:07.421|1001|17|15|9|8|7.9|BATT"), x -> a.add(x));
            assertEquals(1, a.size());
        } catch (Exception e) {
            fail("Unexpected exception");
        }
    }
}
